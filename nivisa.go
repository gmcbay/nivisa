package nivisa

/*
#include "visa.h"
*/
import "C"

import (
	"bytes"
	"fmt"
	"log"
	"syscall"
	"unsafe"
)

// Currently assumes amd64 GOARCH and Windows GOOS by loading NIVISA64.dll.
// If other platforms are required to be supported in the future, this
// will need to change.

var (
	nivisaDLL       *syscall.DLL = syscall.MustLoadDLL("NIVISA64.dll")
	viOpenDefaultRM              = nivisaDLL.MustFindProc("viOpenDefaultRM")
	viFindRsrc                   = nivisaDLL.MustFindProc("viFindRsrc")
	viFindNext                   = nivisaDLL.MustFindProc("viFindNext")
	viSetAttribute               = nivisaDLL.MustFindProc("viSetAttribute")
	viOpen                       = nivisaDLL.MustFindProc("viOpen")
	viClose                      = nivisaDLL.MustFindProc("viClose")
	viRead                       = nivisaDLL.MustFindProc("viRead")
	viWrite                      = nivisaDLL.MustFindProc("viWrite")
)

var resMgr C.ViSession

func initVisaRM() (err error) {
	var status uintptr

	log.Printf("nivisa: Initializing VISA resource manager...\n")

	if status, _, _ = viOpenDefaultRM.Call(uintptr(unsafe.Pointer(&resMgr))); status < C.VI_SUCCESS {
		err = fmt.Errorf("nivisa: Error returned from viOpenDefaultRM: 0x%x", status)
		return
	}

	var numInstrs C.ViUInt32
	var list C.ViFindList
	var desc [C.VI_FIND_BUFLEN]byte

	if status, _, _ = viFindRsrc.Call(uintptr(resMgr),
		uintptr(unsafe.Pointer(C.CString("/?*"))),
		uintptr(unsafe.Pointer(&list)),
		uintptr(unsafe.Pointer(&numInstrs)),
		uintptr(unsafe.Pointer(&desc[0]))); status < C.VI_SUCCESS {

		err = fmt.Errorf("nivisa: Error returned from viFindRsrc: 0x%x", status)
		viClose.Call(uintptr(unsafe.Pointer(&resMgr)))
		return
	}

	for i := 0; i < int(numInstrs); i++ {
		descPrint := bytes.Split(desc[:], []byte{0x0})[0]
		log.Printf("nivisa: Detected Instrument: %s\n", descPrint)
		viFindNext.Call(uintptr(list), uintptr(unsafe.Pointer(&desc[0])))
	}

	return
}

func query(cmd string, equip C.ViSession, maxBufferSize int) (result string, err error) {
	var (
		status   uintptr
		retCount C.ViUInt32
	)

	cmd = cmd + "\r\n"

	log.Printf("nivisa: query cmd: %s", cmd)

	buffer := make([]byte, maxBufferSize)

	if status, _, _ = viWrite.Call(uintptr(equip), uintptr(unsafe.Pointer(C.CString(cmd))), 6,
		uintptr(unsafe.Pointer(&retCount))); status < C.VI_SUCCESS {
		err = fmt.Errorf("nivisa: Unable to execute write on device (%v), status: 0x%x", equip, status)
		return
	}

	if status, _, _ = viRead.Call(uintptr(equip), uintptr(unsafe.Pointer(&buffer[0])), uintptr(maxBufferSize),
		uintptr(unsafe.Pointer(&retCount))); status < C.VI_SUCCESS {
		err = fmt.Errorf("nivisa: Unable to execute read on device (%v), status: 0x%x", equip, status)
		return
	}

	if term := bytes.IndexAny(buffer[:], "\r\n"); term != -1 {
		result = string(buffer[:term])
	} else {
		result = ""
	}

	return
}
