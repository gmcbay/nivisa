package nivisa

/*
#include "visa.h"
*/
import "C"

import (
	"bytes"
	"fmt"
	"log"
	"strconv"
	"strings"
	"unsafe"
)

type Esp300 struct {
	equip C.ViSession
}

const (
	ESP300_BUFFER_SIZE = 200

	NEGATIVE = "-"
	POSITIVE = "+"
)

func NewEsp300(devName string) (esp *Esp300, err error) {
	var (
		status   uintptr
		equip    C.ViSession
		retCount C.ViUInt32
	)

	if resMgr == 0 {
		if err = initVisaRM(); err != nil {
			return
		}
	}

	if status, _, _ = viOpen.Call(uintptr(resMgr),
		uintptr(unsafe.Pointer(C.CString(devName))), 0x4, 99,
		uintptr(unsafe.Pointer(&equip))); status < C.VI_SUCCESS {
		err = fmt.Errorf("nivisa: Unable to open named ESP300 device (%s), status: 0x%x", devName, status)
		return
	}

	// Set 2.5 second timeout

	if status, _, _ = viSetAttribute.Call(uintptr(equip), C.VI_ATTR_TMO_VALUE, 2500); status < C.VI_SUCCESS {
		err = fmt.Errorf("nivisa: Unable to set timeout attribute on device (%s), status: 0x%x", devName, status)
		return
	}

	if status, _, _ = viWrite.Call(uintptr(equip), uintptr(unsafe.Pointer(C.CString("*IDN?\n"))), 6,
		uintptr(unsafe.Pointer(&retCount))); status < C.VI_SUCCESS {
		err = fmt.Errorf("nivisa: Unable to execute IDN query on device (%s), status: 0x%x", devName, status)
		return
	}

	var buffer [ESP300_BUFFER_SIZE]byte

	if status, _, _ = viRead.Call(uintptr(equip), uintptr(unsafe.Pointer(&buffer[0])), ESP300_BUFFER_SIZE,
		uintptr(unsafe.Pointer(&retCount))); status < C.VI_SUCCESS {
		err = fmt.Errorf("nivisa: Unable to execute IDN query on device (%s), status: 0x%x", devName, status)
		return
	}

	bufferStr := string(bytes.Split(buffer[:], []byte{0x0})[0])

	if !strings.Contains(bufferStr, "ESP300") {
		err = fmt.Errorf("nivisa: Device (%s) does not appear to be an ESP300 Motion Controller", devName)
		return
	}

	log.Printf("nivisa: IDN: %s\n", bufferStr)

	esp = &Esp300{equip: equip}

	return
}

func (esp *Esp300) Position(axisNum int) (pos float32, err error) {
	var result string

	if result, err = query(fmt.Sprintf("%dTP", axisNum), esp.equip, ESP300_BUFFER_SIZE); err == nil {
		var posFloat64 float64

		if posFloat64, err = strconv.ParseFloat(result, 32); err == nil {
			pos = float32(posFloat64)
		}
	}

	err = esp.checkForError()

	return
}

func (esp *Esp300) Velocity(axisNum int) (velocity float32, err error) {
	var result string

	if result, err = query(fmt.Sprintf("%dVA?", axisNum), esp.equip, ESP300_BUFFER_SIZE); err == nil {
		var velocityFloat64 float64

		if velocityFloat64, err = strconv.ParseFloat(result, 32); err == nil {
			velocity = float32(velocityFloat64)
		}
	}

	err = esp.checkForError()

	return
}

func (esp *Esp300) SetVelocity(axisNum int, velocity float32) (err error) {
	if _, err = query(fmt.Sprintf("%dVA%f", axisNum, velocity), esp.equip, ESP300_BUFFER_SIZE); err != nil {
		return
	}

	err = esp.checkForError()

	return
}

func (esp *Esp300) MotorOn(axisNum int) (pos float32, err error) {
	if _, err = query(fmt.Sprintf("%dMO", axisNum), esp.equip, ESP300_BUFFER_SIZE); err != nil {
		return
	}

	err = esp.checkForError()

	return
}

func (esp *Esp300) MoveToTravelLimit(axisNum int, direction string) (pos float32, err error) {
	if _, err = query(fmt.Sprintf("%dMT%s", axisNum, direction), esp.equip, ESP300_BUFFER_SIZE); err != nil {
		return
	}

	err = esp.checkForError()

	return
}

func (esp *Esp300) MoveToAbsPos(axisNum int, pos float32) (err error) {
	// TODO : needs more work to ensure desired position is achived,
	// often off by 0.1 ?

	pos += 0.1

	if _, err = query(fmt.Sprintf("%dPA%f", axisNum, pos), esp.equip, ESP300_BUFFER_SIZE); err != nil {
		return
	}

	err = esp.checkForError()

	return
}

func (esp *Esp300) SendCmd(cmd string) (err error) {
	if _, err = query(cmd, esp.equip, ESP300_BUFFER_SIZE); err != nil {
		return
	}

	err = esp.checkForError()

	return
}

func (esp *Esp300) checkForError() (err error) {
	var errorResult string

	if errorResult, err = query("TB?", esp.equip, ESP300_BUFFER_SIZE); err != nil {
		return
	}

	if errorResult[0:3] != "0, " {
		err = fmt.Errorf("nsvisa: Device-specific error occured on device (%v), err: %v\n", esp, errorResult)
	}

	return
}
